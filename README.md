# OpenML dataset: Texas-Winter-Storm-2021-Tweets

https://www.openml.org/d/43670

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Winter Storm Uri in February 2021 caused havoc across the United States and specifically to Texas involving mass power outages, water and food shortages, and dangerous weather conditions. 
This dataset consists of 23K+ tweets during the crisis week. Data is filtered to mostly include the tweets from influencers (users having more than 5000 followers) however there is a small subset of tweets from other users as well.
My notebook - https://www.kaggle.com/rajsengo/eda-texas-winterstrom-2021-tweets
Acknowledgements

https://www.kaggle.com/gpreda/pfizer-vaccine-tweets - For the inspiration
https://github.com/dataquestio/twitter-scrape - Reference utility to scrape twitter 

Inspiration
Apply NLP techniques to undestand user sentiments about the crisis management

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43670) of an [OpenML dataset](https://www.openml.org/d/43670). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43670/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43670/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43670/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

